#!/bin/bash

# Settings for Vhost
# ------------------
# 1. Create Folder /etc/httpd/conf/vhosts
#
# 2. Change permission of folder /srv/http
#
# 3. Create Folder /srv/http/default
#
# 4. Open /etc/httpd/conf/http.conf
#    a. Edit Basic Details and enable needed packages
#    b. Change Line 'DocumentRoot "/srv/http"' into 'DocumentRoot "/srv/http/default"'
#    c. Change Line '<Directory "/srv/http">' into '<Directory "/srv/http/default">'
#    d. Make AllowOverride 'All' from 'None' under above Directory
#    e. Uncomment '#Include conf/extra.httpd-vhosts.conf'
#    f. Move this line to bottom
#
# 5. Open /etc/httpd/conf/extra/httpd-vhosts.conf
#    a. Comment Everything
#    b. Add below code at bottom
#		<VirtualHost *:80>
#			ServerAdmin amjid@coderythm.com
#			DocumentRoot /srv/http/default
#			ServerName localhost.com
#			ServerAlias www.localhost.com
#		</VirtualHost>
#
# 6. Restart Server : sudo systemctl restart httpd

# For Virtual Host
# ----------------
# 1. sudo mkdir /srv/http/example.local
#    sudo mkdir /srv/http/example.local/public_html
#    sudo mkdir /srv/http/example.local/logs
#
# 2. sudo gedit /etc/httpd/conf/vhosts/example.local
# 	 <VirtualHost *:80>
# 	    ServerAdmin amjid@coderythm.com
# 	    DocumentRoot "/srv/http/example.local/public_html"
# 	    ServerName example.local
# 	    ServerAlias example.local
# 	    ErrorLog "/srv/http/example.local/logs/example.local-error_log"
# 	    CustomLog "/srv/http/example.local/logs/example.local-access_log" common
# 
# 	    <Directory "/srv/http/example.local/public_html">
# 		Require all granted
# 	    </Directory>
# 	 </VirtualHost>
# 	
# 3. sudo gedit /etc/httpd/conf/httpd.conf
# 	 #Enabled Vhosts:
# 	 Include conf/vhosts/example.local
# 
# 4. sudo gedit /etc/hosts
# 	 127.0.0.1	www.example.local	example.local
#
# 5. sudo systemctl restart httpd
#
# 6. sudo apachectl configtest

httpfile='/etc/httpd/conf/httpd.conf'
httploc='/srv/http/'
defaultfolder='/srv/http/default'
vhostpdir='/etc/httpd/conf/vhosts'

result=$(ls -l | grep "/srv/http/default" $httpfile)

if [ -z "$result" ]; then	
	if [ ! -d $vhostpdir ]; then
		mkdir $vhostpdir
	fi
	chmod -R 777 $httploc
	if [ ! -d $defaultfolder ]; then
		mkdir $defaultfolder
		touch $defaultfolder'/index.php'
		echo '<?php phpinfo(); ?>' >> $defaultfolder'/index.php'
	fi
	clear
	echo 'Initial setup for vhost'
	echo 'Please complete below steps for Continuing'
	echo
	echo 'Step 1: Open /etc/httpd/conf/httpd.conf'
	echo '	a. Edit Basic Details and enable needed packages'
	echo '	b. Change Line DocumentRoot "/srv/http" into DocumentRoot "/srv/http/default"'
	echo '	c. Change Line <Directory "/srv/http"> into <Directory "/srv/http/default">'
	echo '	d. Make AllowOverride All from None under above Directory'
	echo '	e. Uncomment #Include conf/extra.httpd-vhosts.conf'
	echo '	f. Move this line to bottom'
	echo

	echo 'Step 2: Open /etc/httpd/conf/extra/httpd-vhosts.conf'
	echo '	a. Comment Everything'
	echo '	b. Add below code at bottom'
	echo '	<VirtualHost *:80>'
	echo '		ServerAdmin email@domain.com'
	echo '		DocumentRoot /srv/http/default'
	echo '		ServerName localhost.com'
	echo '		ServerAlias www.localhost.com'
	echo '	</VirtualHost>'

	echo 
	echo 'Step 3: Restart Server : sudo systemctl restart httpd'

	echo 
	echo 'Step 4: check for any Errors : sudo apachectl configtest'
else
	echo "Enter Domain Name : ";read domainnameoriginal
	domainname=$domainnameoriginal'.local' 

	# Creating the domain folders
	if [ ! -d $httploc$domainname ]; then
		mkdir $httploc$domainname
	fi
	echo $httploc$domainname' Created'
	if [ ! -d $httploc$domainname'/public_html' ]; then
		mkdir $httploc$domainname'/public_html'
	fi
	echo $httploc$domainname'/public_html Created'
	if [ ! -d $httploc$domainname'/logs' ]; then
		mkdir $httploc$domainname'/logs'
	fi
	echo $httploc$domainname'/logs Created'

	# Virtual Host file Creating
	echo
	vhostdir='/etc/httpd/conf/vhosts/'$domainname
	if [ ! -f $vhostdir ]; then
		touch $vhostdir
	fi
	echo $vhostdir' Created'

	# Virtual Host File code Adding
	echo '<VirtualHost *:80>' >> $vhostdir
	printf '\t ServerAdmin amjid@coderythm.com' >> $vhostdir
	echo >> $vhostdir
	printf '\t DocumentRoot "/srv/http/%s/public_html"' $domainname >> $vhostdir
	echo >> $vhostdir
	printf '\t ServerName %s' $domainname >> $vhostdir
	echo >> $vhostdir
	printf '\t ServerAlias %s' $domainname >> $vhostdir
	echo >> $vhostdir
	printf '\t ErrorLog "/srv/http/%s/logs/%s-error_log"' $domainname $domainname >> $vhostdir
	echo >> $vhostdir
	printf '\t CustomLog "/srv/http/%s/logs/%s-access_log" common' $domainname $domainname >> $vhostdir
	echo >> $vhostdir
	printf '\t <Directory "/srv/http/%s/public_html">' $domainname >> $vhostdir
	echo >> $vhostdir
	printf '\t \t Require all granted' >> $vhostdir
	echo >> $vhostdir
	printf '\t \t AllowOverride All' >> $vhostdir
	echo >> $vhostdir
	printf '\t </Directory>' >> $vhostdir
	echo >> $vhostdir
	echo '</VirtualHost>' >> $vhostdir

	echo 'Details Wriiten into '$vhostdir

	# Add Virtual link to host main files
	echo >> '/etc/httpd/conf/httpd.conf'
	printf "Include conf/vhosts/%s" $domainname >> '/etc/httpd/conf/httpd.conf'
	echo 'Link to virtual host added'

	# Virtual host added to Host
	echo >> /etc/hosts
	printf "127.0.0.1\t%s\t%s" $domainname $domainnameoriginal >> '/etc/hosts'
	echo 'Host address added in Hosts File'

	# Restart Server
	echo
	echo 'Restarting Server...'
	systemctl restart httpd
	echo 'Done.'

	# Testing for errors
	echo
	echo 'Checking for Errors...'
	apachectl configtest

	echo
	# Change Folder permission
	echo 'Created Domain : '$domainname
	chmod -R 777 '/srv/http/'$domainname

	# Initial File for Server
	if [ ! -f $httploc$domainname'/public_html/index.html' ]; then
		touch $httploc$domainname'/public_html/index.html'
	fi
	echo 'Welcome To '$domainname >> $httploc$domainname'/public_html/index.html'
fi